// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import { Lazyload } from 'vant';
import api from './api'
import store from './store'

Vue.use(Lazyload)

Vue.prototype.$api = api;

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
    el: '#app',
    router,
    store,
    components: { App },
    template: '<App/>'
})

//记录访问ip和城市
let ipdata = {
    ip: returnCitySN["cip"],
    city: returnCitySN["cname"]
}
api("/api/ip", ipdata, 'POST')