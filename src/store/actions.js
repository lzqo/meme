/*
通过mutation间接更新state的多个方法的对象
 */
import api from "../api";

export default {
  // 异步获取表情包
  async getList({ commit, state }, {name,type1,type2}) {
    return new Promise(function(resolve, reject) {
      // 发送异步ajax请求
      api(`/api/${name}Img`,{type1:type1,type2:type2}).then(res => {
        let result = res;
        commit(name+type1+type2, result);// 提交一个mutation
        resolve(res);
      });
    });
  }
};
