/*
直接更新state的多个方法的对象
 */

export default {
  memefunny(state, list) {
    state.obj.memefunny = list
  },
  memefunny20210906(state, list) {
    state.obj.memefunny20210906 = list
  },
  memefunny20210910(state, list) {
    state.obj.memefunny20210910 = list
  },
  memefunny20210918(state, list) {
    state.obj.memefunny20210918 = list
  },
  memefunny20210929(state, list) {
    state.obj.memefunny20210929 = list
  },
  memefunny20211012(state, list) {
    state.obj.memefunny20211012 = list
  },
  memefunny20211024(state, list) {
    state.obj.memefunny20211024 = list
  },
  memefunny20211201(state, list) {
    state.obj.memefunny20211201 = list
  },
  // memefestival(state, list) {
  //   state.obj.memefestival = list
  // },
  // memeguoqing(state, list) {
  //   state.obj.memeguoqing = list
  // },
  memedengjiaqi(state, list) {
    state.obj.memedengjiaqi = list
  },
  mememild(state, list) {
    state.obj.mememild = list
  },
  memecp(state, list) {
    state.obj.memecp = list
  },
  memedog(state, list) {
    state.obj.memedog = list
  },
  avatargirlreal(state, list) {
    state.obj.avatargirlreal = list
  },
  avatargirlcartoon(state, list) {
    state.obj.avatargirlcartoon = list
  },
  avatargirlother(state, list) {
    state.obj.avatargirlother = list
  },
  avatarboyreal(state, list) {
    state.obj.avatarboyreal = list
  },
  avatarboycartoon(state, list) {
    state.obj.avatarboycartoon = list
  },
  avatarboyother(state, list) {
    state.obj.avatarboyother = list
  }
}