import Vue from 'vue'
import Router from 'vue-router'

import Meme from '../pages/Meme/Meme.vue'
import Avatar from '../pages/Avatar/Avatar.vue'
import Connect from '../pages/Connect/Connect.vue'
import ErrorPage from '../pages/ErrorPage.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  scrollBehavior: () => ({ y: 0 }), //路由跳转后页面回到顶部
  // scrollBehavior: (to,from,savedPosition) => {//记录位置，但是对于通过<router-link>跳转的不行
  //   console.log('savedPosition',savedPosition)
  //   return savedPosition //{x: 0, y: 700}
  // },
  routes: [
    {
      path: '/meme',
      component: Meme
    },
    {
      path: '/avatar',
      component: () =>
          import(/* webpackChunkName: "avatar" */ "../pages/Avatar/Avatar.vue")//异步加载组件，这样可以减少首屏时间（相当于懒加载）。因为一打开页面时先只加载首页的组件，这些import的组件等用到再加载。但是有一个问题是如果直接进入懒加载的页面，会把非懒加载的也加载，但是用户一般都是从首屏进入的，所以对追求首屏性能来说这个问题可以忽略。如果首页也设置懒加载的话，会影响首页加载速度，因为懒加载是不优先加载的。
    },
    {
      path: '/connect',
      component: () =>
          import(/* webpackChunkName: "connect" */ "../pages/Connect/Connect.vue")
    },
    {
      path: '/',
      redirect: '/meme'
    },
    {
      path: "*", // 这个路由可以匹配所有的路径 /list
      name: "ErrorPage",
      component: () =>
          import(/* webpackChunkName: "errorPage" */ "../pages/ErrorPage.vue")
    }
  ]
})
